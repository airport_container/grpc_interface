auth:
	protoc --go_out=gosdk/auth proto/auth/auth.proto --go-grpc_out=gosdk/auth proto/auth/auth.proto
	protoc --grpc-gateway_out=logtostderr=true,grpc_api_configuration=proto/auth/auth_http.yaml:\
	gosdk/auth  proto/auth/auth.proto

record:
	protoc --go_out=gosdk/record  proto/record/record.proto  --go-grpc_out=gosdk/record  proto/record/record.proto
	protoc --grpc-gateway_out=logtostderr=true,grpc_api_configuration=proto/record/record_http.yaml:\
	gosdk/record  proto/record/record.proto
	
container:
	protoc --go_out=gosdk/container  proto/container/container.proto  --go-grpc_out=gosdk/container  proto/container/container.proto
	protoc --grpc-gateway_out=logtostderr=true,grpc_api_configuration=proto/container/container_http.yaml:\
	gosdk/container  proto/container/container.proto