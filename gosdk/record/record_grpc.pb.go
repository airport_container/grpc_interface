// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.20.3
// source: proto/record/record.proto

package record

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	Record_SearchKey_FullMethodName  = "/record.Record/SearchKey"
	Record_ListRecord_FullMethodName = "/record.Record/ListRecord"
	Record_DashBoard_FullMethodName  = "/record.Record/DashBoard"
)

// RecordClient is the client API for Record service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RecordClient interface {
	SearchKey(ctx context.Context, in *ListRecordRequest, opts ...grpc.CallOption) (*SearchKeyResponse, error)
	ListRecord(ctx context.Context, in *ListRecordRequest, opts ...grpc.CallOption) (*ListRecordResponse, error)
	DashBoard(ctx context.Context, in *DashBoardRequest, opts ...grpc.CallOption) (*DashBoardRResponse, error)
}

type recordClient struct {
	cc grpc.ClientConnInterface
}

func NewRecordClient(cc grpc.ClientConnInterface) RecordClient {
	return &recordClient{cc}
}

func (c *recordClient) SearchKey(ctx context.Context, in *ListRecordRequest, opts ...grpc.CallOption) (*SearchKeyResponse, error) {
	out := new(SearchKeyResponse)
	err := c.cc.Invoke(ctx, Record_SearchKey_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recordClient) ListRecord(ctx context.Context, in *ListRecordRequest, opts ...grpc.CallOption) (*ListRecordResponse, error) {
	out := new(ListRecordResponse)
	err := c.cc.Invoke(ctx, Record_ListRecord_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *recordClient) DashBoard(ctx context.Context, in *DashBoardRequest, opts ...grpc.CallOption) (*DashBoardRResponse, error) {
	out := new(DashBoardRResponse)
	err := c.cc.Invoke(ctx, Record_DashBoard_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RecordServer is the server API for Record service.
// All implementations must embed UnimplementedRecordServer
// for forward compatibility
type RecordServer interface {
	SearchKey(context.Context, *ListRecordRequest) (*SearchKeyResponse, error)
	ListRecord(context.Context, *ListRecordRequest) (*ListRecordResponse, error)
	DashBoard(context.Context, *DashBoardRequest) (*DashBoardRResponse, error)
	mustEmbedUnimplementedRecordServer()
}

// UnimplementedRecordServer must be embedded to have forward compatible implementations.
type UnimplementedRecordServer struct {
}

func (UnimplementedRecordServer) SearchKey(context.Context, *ListRecordRequest) (*SearchKeyResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchKey not implemented")
}
func (UnimplementedRecordServer) ListRecord(context.Context, *ListRecordRequest) (*ListRecordResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListRecord not implemented")
}
func (UnimplementedRecordServer) DashBoard(context.Context, *DashBoardRequest) (*DashBoardRResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DashBoard not implemented")
}
func (UnimplementedRecordServer) mustEmbedUnimplementedRecordServer() {}

// UnsafeRecordServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RecordServer will
// result in compilation errors.
type UnsafeRecordServer interface {
	mustEmbedUnimplementedRecordServer()
}

func RegisterRecordServer(s grpc.ServiceRegistrar, srv RecordServer) {
	s.RegisterService(&Record_ServiceDesc, srv)
}

func _Record_SearchKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecordServer).SearchKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Record_SearchKey_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecordServer).SearchKey(ctx, req.(*ListRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Record_ListRecord_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecordServer).ListRecord(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Record_ListRecord_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecordServer).ListRecord(ctx, req.(*ListRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Record_DashBoard_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DashBoardRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RecordServer).DashBoard(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Record_DashBoard_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RecordServer).DashBoard(ctx, req.(*DashBoardRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Record_ServiceDesc is the grpc.ServiceDesc for Record service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Record_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "record.Record",
	HandlerType: (*RecordServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchKey",
			Handler:    _Record_SearchKey_Handler,
		},
		{
			MethodName: "ListRecord",
			Handler:    _Record_ListRecord_Handler,
		},
		{
			MethodName: "DashBoard",
			Handler:    _Record_DashBoard_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/record/record.proto",
}
